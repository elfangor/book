# Download

Veloren is a cross-platform game and can run on Windows, Linux and Mac OS.

If you're only interested in binary builds of the game, check the website at <https://www.veloren.net>.

(We currently don't provide official downloads of Veloren, you have to download the source code and compile it yourself, the rest of this chapter will explain how to do that.)
