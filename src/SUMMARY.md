# This book is used by developers and users
# Yet, we should sort it to make it easy for new users to try it out. We don't want to scare them with to much contributing info
# Developers and power users know the book already and will find their section quite quickly after some time

- [Introduction](introduction/README.md)
  - [What is Veloren?](introduction/what.md)
  - [Who develops Veloren?](introduction/who.md)
- [Download](download/README.md)
  - [Toolchain](download/toolchain.md)
  - [Compiling](download/compiling.md)
- [Contribute](contribute/README.md)
  - [Getting Started](contribute/getting-started.md)
  - [Git Workflow](contribute/workflow.md)
  - [Project Structure](contribute/project-structure.md)
  - [RFCs](contribute/rfcs.md)
  - [Generating Docs](contribute/generating-docs.md)
  - [Development FAQ](contribute/development-faq.md)
  - [Extend this Book](contribute/extend-this-book.md)
